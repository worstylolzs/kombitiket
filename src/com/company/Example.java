package com.company;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Example {
    public static List<String> texty(String[] arr, int len) {
        List<String> result = new ArrayList<String>();
        long n = arr.length; //n=4
        long N = (long) Math.pow(2, n); // 2 na 4 = 16
        for (long i = 0; i < N; i++) {
            String code = Long.toBinaryString(N | i);
            code=code.substring(1);
            String b = "";
            long counter = 0;
            for (int j = 0; j < n; j++) {
                if (code.charAt(j) == '1') {
                    b += arr[j];
                    counter++;
                }
            }
            if (counter == len) {
                result.add(b);

            }
        }
        return result;
    }

    public static float round3(float d, int decimalPlace) {
        return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
    }
}
