package com.company;

import java.util.List;
import java.util.Scanner;

public class Kombitiket {
    public void kombiTiketPlay(){
    Scanner sc = new Scanner(System.in);
    String pismena[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"};
        System.out.println("Skupin může být maximálně 15, zadejte tedy číslo v rozmezí 1-15");
        System.out.println("Zadejte počet skupin:");
    int podminkavstupu = 1;
    String vstupcisel = null;
    int pocetskupin = 0;
        while (podminkavstupu == 1) {
        vstupcisel = sc.nextLine();
        if (vstupcisel.matches(".*\\d+.*")) {
            pocetskupin = Integer.parseInt(vstupcisel);
            if (pocetskupin > 0 && pocetskupin <= 15) {
                podminkavstupu = 2;
            } else {
                System.out.println("Zadejte prosím číslo mezi 1-15");
            }
        } else {
            System.out.println("Zadal jste písmeno, zadejte prosím číslo.");
        }
    }
    String arr[] = new String[pocetskupin];
    float vkladnaskupinu[] = new float[pocetskupin];
        for (int i2 = 0; i2 < pocetskupin; i2++) {
        arr[i2] = pismena[i2];
    }
        System.out.println("Zadejte kurzy, na jednotlivé skupiny");
    String tre[] = new String[pocetskupin];
        for (int i3 = 0; i3 < pocetskupin; i3++) {
        System.out.println("Zadejte kurz ke skupině:" + pismena[i3] + " Ve formátu x.xx");
        String pojistka = sc.nextLine();
        while (!pojistka.contains(".")) {
            System.out.println("prosím zadejte číslo ve formátu x.xx a ne písmeno");
            pojistka = sc.nextLine();
        }
        tre[i3] = pojistka + "#";
    }
        for (int i4 = 0; i4 < pocetskupin; i4++) {
        System.out.println("Zadejte vklad ke skupině:" + pismena[i4] + " Ve formátu x.xx");
        String pojistka = sc.nextLine();
        Scanner scanpojistky = new Scanner(pojistka);
        while (!pojistka.contains(".")) {
            System.out.println("prosím zadejte číslo ve formátu x.xx a ne písmeno");
            pojistka = sc.nextLine();
        }
        float vklad = Float.parseFloat(pojistka);
        vkladnaskupinu[i4] = vklad;
    }
    float CelkovyVklad = 0;
    float CelkovyKurz = 0;
    float CelkovaVyhra = 0;
    int PocetOpakuProFor = 1;
    int PocetOpakuPropole = 0;
        for (int i5 = 0; i5 < pocetskupin; i5++) {
        int len = PocetOpakuProFor;
        String tokens = "";
        String dokens = "";
        List<String> output = Example.texty(arr, len);
        List<String> popud = Example.texty(tre, len);
        float nasobvsechkurzu = 1;
        float KurzJednohoPismena[] = new float[pocetskupin];
        for (int i = 0; i < output.size(); i++) {
            tokens = popud.get(i);
            String[] part = tokens.split("#");
            for (int d = 0; d < part.length; d++) {
                float PomocnikNasobeniKurzu = 1;
                KurzJednohoPismena[d] = Float.parseFloat(part[d]);
                for (int p = 0; p < part.length; p++) {
                    PomocnikNasobeniKurzu = PomocnikNasobeniKurzu * KurzJednohoPismena[p];
                    nasobvsechkurzu = Example.round3(PomocnikNasobeniKurzu, 2);
                }
            }
            float vyhra = nasobvsechkurzu * vkladnaskupinu[PocetOpakuPropole];
            vyhra = Example.round3(vyhra, 4);
            CelkovyVklad = CelkovyVklad + vkladnaskupinu[PocetOpakuPropole];
            CelkovaVyhra = CelkovaVyhra + vyhra;
            System.out.println(output.get(i) + "----------celkový kurz:" + nasobvsechkurzu + "----------vklad:" + vkladnaskupinu[PocetOpakuPropole] + "----------výhra:" + vyhra/*Example.round3(nasobvsechkurzu,2)*/);
        }
        PocetOpakuProFor = PocetOpakuProFor + 1;
        PocetOpakuPropole = PocetOpakuPropole + 1;
    }
    CelkovyKurz = Example.round3(CelkovaVyhra / CelkovyVklad, 2);
    CelkovaVyhra = Example.round3(CelkovaVyhra, 2);
        if (CelkovaVyhra <= 9999999) {
        System.out.println("Celková výhra tiketu je:" + CelkovaVyhra + "----------Celkový kurz je:" + CelkovyKurz);
    } else {
        System.out.println("Celková výhra tiketu je větší než 10 milionů, tím pádem nelze vypsat." + "----------Celkový kurz je:" + CelkovyKurz);
    }
}
}
