package com.company;

import java.sql.SQLOutput;

public class Combination {

    /* arr[]  ---> Input Array
    data[] ---> Temporary array to store current combination
    start & end ---> Staring and Ending indexes in arr[]
    index  ---> Current index in data[]
    r ---> Size of a combination to be printed */

    public Combination(float[] nactenaCisla, float finalniCislo) {
        this.nactenaCisla = nactenaCisla;
        this.finalniCislo = finalniCislo;
    }

    static float[] nactenaCisla;
    static float finalniCislo;

    static void combinationUtil(int arr[], int n, int r, int index,
                                int data[], int i)
    {
        // Current combination is ready to be printed, print it
        if (index == r)
        {
            String insideFor = "";
            float soucetCisel=0.00f;
            for (int j=0; j<r; j++) {
                soucetCisel+=nactenaCisla[j];
                insideFor+=data[j] + " ";
            }
            String worker1=String.valueOf(soucetCisel);
            String worker2=String.valueOf(finalniCislo);
            System.out.println(worker1+"/"+worker2);;
//            if (worker1.contains(worker2)){
                System.out.println(insideFor);
//            }
//            System.out.println("");
            return;
        }

        // When no more elements are there to put in data[]
        if (i >= n)
            return;

        // current is included, put next at next location
        data[index] = arr[i];

        combinationUtil(arr, n, r, index+1, data, i+1);


        // current is excluded, replace it with next (Note that
        // i+1 is passed, but index is not changed)
        combinationUtil(arr, n, r, index, data, i+1);


    }

    // The main function that prints all combinations of size r
    // in arr[] of size n. This function mainly uses combinationUtil()
    static void printCombination(int arr[], int n, int r)
    {
        // A temporary array to store all combination one by one
        int data[]=new int[r];

        // Print all combination using temporary array 'data[]'
        combinationUtil(arr, n, r, 0, data, 0);
    }

    /*Driver function to check for above function*/
    public static void main (String[] args) {
        int arr[] = {1, 2, 3, 4, 5};
        int r = 3;
        int n = arr.length;
        printCombination(arr, n, r);
    }
}
