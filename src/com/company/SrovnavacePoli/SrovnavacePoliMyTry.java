package com.company.SrovnavacePoli;

import java.util.Random;

public class SrovnavacePoliMyTry {
    static int[] srovnanePole(int[] poleVlozenychCisel){
        int nejmensiCislo = 0;
        int poleNejmensihoCisla=0;
        boolean neniMensiCislo;
        String stringNaSrovnanaCisla="";
        int cisloPoleVlozenychCisel=0;
        while (cisloPoleVlozenychCisel!=poleVlozenychCisel.length) {
            neniMensiCislo=true;
            while (neniMensiCislo) {
                nejmensiCislo=poleVlozenychCisel[cisloPoleVlozenychCisel];
                for (int i = cisloPoleVlozenychCisel; i < poleVlozenychCisel.length; i++) {
                    if (nejmensiCislo >= poleVlozenychCisel[i]) {
                        nejmensiCislo = poleVlozenychCisel[i];
                        poleNejmensihoCisla=i;
                        neniMensiCislo=false;
                    }
                }
            }
            stringNaSrovnanaCisla+=nejmensiCislo+";";
            System.out.print(nejmensiCislo+";");
            int vymenik1=poleVlozenychCisel[poleNejmensihoCisla];
            int vymenik2=poleVlozenychCisel[cisloPoleVlozenychCisel];
            poleVlozenychCisel[poleNejmensihoCisla]=vymenik2;
            poleVlozenychCisel[cisloPoleVlozenychCisel]=vymenik1;
            cisloPoleVlozenychCisel++;
        }
        String[] poleCisel=stringNaSrovnanaCisla.split(";");
        for (int i = 0; i < poleCisel.length; i++) {
            poleVlozenychCisel[i]=Integer.parseInt(poleCisel[i]);
        }
        System.out.println("");
        return poleVlozenychCisel;
    }

    public static void main(String[] args) {
        int[] nahodneCisla=new int[15];
        Random random = new Random();
        for (int i = 0; i < nahodneCisla.length; i++) {
            nahodneCisla[i]=random.nextInt(1000);
            System.out.print(nahodneCisla[i]+";");
        }
        System.out.println("");
        nahodneCisla=srovnanePole(nahodneCisla);
        System.out.print("srovnaná čísla=");
        for (int i = 0; i < nahodneCisla.length; i++) {
            System.out.print(nahodneCisla[i]+";");
        }
        System.out.println("");
    }
}
